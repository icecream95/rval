# rval

`rval` is an incredibly simple library for easily changing rvalues in
a C or C++ program at runtime.

## How it works

When the `rval` function is called with name `NAME`, it will try
reading a uint64_t value from the file `/tmp/rval.NAME`. If it
succeeds, that value will be returned from the function; otherwise,
the default value is returned. `strtoull` is used for parsing, which
supports hexadecimal and octal constants.

`rval_f` is similar, though it reads a `double` value instead.

The value is reread on each call to `rval`, so the value can be
changed during execution.

## Installation

Copy `rval.h` to `/usr/local/include` or somewhere in your project's
include directory.

## Example

This example program is also in the file `example.c`:

```c
#include <stdio.h>
#include <inttypes.h>

#include "rval.h"

int main()
{
    uint64_t foo = rval("foo", 2);
    double bar = rval_f("bar", 1.6);

    printf("foo is %"PRIx64" and bar is %f\n", foo, bar);
}
```

Try running it; it will print `foo is 2 and bar is 1.600000`.

Use the `write.sh` script to change `foo`:

```sh
$ ./write.sh foo 10
```

And use a redirected echo to change `bar`:

```sh
$ echo 21.5 >/tmp/rval.bar
```

Now, the program will print `foo is 10 and bar is 21.500000`.

The `write.sh` script can also be used interactively: just run
`./write.sh foo` and enter values to continue changing `foo`, then
exit with Ctrl-D or Ctrl-C.
