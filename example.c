#include <stdio.h>
#include <inttypes.h>

#include "rval.h"

int main()
{
    uint64_t foo = rval("foo", 2);
    double bar = rval_f("bar", 1.6);

    printf("foo is %"PRIu64" and bar is %f\n", foo, bar);
}
