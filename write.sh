#!/bin/sh

if ! [ "$1" ] || [ "$1" = -h ] || [ "$1" = --help ]; then
    echo USAGE: "$0 NAME [VALUE]"
    echo Values are read from stdin, or the second argument if present
    exit 1
fi

if [ "$2" ]; then
    printf "%s\n" "$2" >/tmp/rval."$1"
    exit 0
fi

while read i
do
    printf "%s\n" "$i" >/tmp/rval."$1"
done
