/* SPDX-License-Identifier: CC0-1.0 */
/* Copyright (C) Icecream95 2021 */

#ifndef RVAL_H_INCLUDE_GUARD
#define RVAL_H_INCLUDE_GUARD

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/file.h>
#include <time.h>
#include <unistd.h>

static inline
uint64_t rval(const char *name, uint64_t def)
{
    uint64_t ret = def;
    char buf[30] = {0};

    char *fname = (char *)malloc(strlen(name) + 20);
    sprintf(fname, "/tmp/rval.%s", name);

    FILE *f = fopen(fname, "r");
    if (f) {
        fread(buf, 29, 1, f);
        ret = strtoull(buf, NULL, 0);
        fclose(f);
    }
    free(fname);

    return ret;
}

#define rval_u rval

static inline
double rval_f(const char *name, double def)
{
    double ret = def;

    char *fname = (char *)malloc(strlen(name) + 20);
    sprintf(fname, "/tmp/rval.%s", name);

    FILE *f = fopen(fname, "r");
    if (f) {
        fscanf(f, "%lf", &ret);
        fclose(f);
    }
    free(fname);

    return ret;
}

static inline
bool rval_bisect(const char *name)
{
    char buf[30] = {0};

    char *fname = (char *)malloc(strlen(name) + 20);
    sprintf(fname, "/tmp/rval.%s", name);

    bool ret = true;

    FILE *f = fopen(fname, "r+");
    if (f) {
        int64_t value = 0;
        fscanf(f, "%"PRId64, &value);
        if (value) {
            int fd = fileno(f);
            flock(fd, LOCK_EX);

            rewind(f);
            fflush(f);
            fread(buf, 29, 1, f);
            value = strtoll(buf, NULL, 0);

            --value;

            ftruncate(fd, 0);
            rewind(f);
            fprintf(f, "%"PRId64"\n", value);
        } else {
            ret = false;
        }

        fclose(f);
    }
    free(fname);

    return ret;
}


/* TODO: Floating-point version */
static inline
uint64_t rval_env(const char *name, uint64_t def)
{
    char *env_name = (char *)malloc(strlen(name) + 20);
    sprintf(env_name, "RVAL_%s", name);

    char *buf = getenv(env_name);
    free(env_name);

    if (buf)
        return strtoull(buf, NULL, 0);
    else
        return def;
}

#define COUNT do { static int count = 0; printf("%s: %i\n", __func__, ++count); } while (0)

#define TIME_START(name) clock_gettime(CLOCK_MONOTONIC, &name)
#define TIME_BEGIN(name) struct timespec name; TIME_START(name)
#define TIME_END(name, print) do { \
        struct timespec end; \
        clock_gettime(CLOCK_MONOTONIC, &end); \
        end.tv_sec -= name.tv_sec; \
        end.tv_nsec -= name.tv_nsec; \
        if (end.tv_nsec < 0) { \
            --end.tv_sec; \
            end.tv_nsec += 1000000000; \
        } \
        printf("%s: %i.%06i\n", print, (int)end.tv_sec, (int)end.tv_nsec / 1000); \
    } while (0)

#endif
